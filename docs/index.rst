metronotesd
==================================================

Metronotes is a protocol for the creation and use of decentralised financial instruments such as asset
exchanges, contracts for difference and dividend payments. It uses Bitcoin as a transport layer.
The Metronotes protocol specification may be found `here <https://github.com/MetronotesXMN/Metronotes>`__.

``metronotesd`` is the reference client (and server daemon) implementation of the Metronotes protocol.

To get ``metronotesd`` installed and set up on your computer, you have two options:

- Set it up manually, using the instructions `here <https://github.com/MetronotesXMN/metronotesd/blob/master/README.md>`__
- For Windows and Ubuntu Linux users, you can use the `automated build system <http://metronotesd-build.rtfd.org>`__


Table of Contents
------------------

.. toctree::
   :maxdepth: 2

   API


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

